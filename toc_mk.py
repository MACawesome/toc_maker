import os, glob, subprocess, sys

def_header="""\\documentclass{article} \n 
\\usepackage{float}
\\usepackage{parskip}
\\usepackage{mathtools}
\\usepackage{pdfpages}
\\usepackage{hyperref}
\n
\\begin{document}
\\tableofcontents
\\newpage"""

def_tail = "\\end{document}"

cur_dir = os.getcwd()

class ltx_doc:
    def __init__(self, header=def_header, tail=def_tail,
                 path=cur_dir, source=None):
        self.header = def_header
        self.tail = def_tail
        self.path = path
        self.content = self.header + '\n' + self.tail
        self.source = str(source)
    def update_content(self, new_text=None):
        self.header = self.header + '\n' + new_text
        self.content = self.header + '\n' + self.tail
    
    def export_pdf(self, filename='volume.tex'):
        out_path = self.path + '/' + filename
        with open(out_path, 'w') as f:
            f.write(self.content)

        # Launch pdflatex
        # Run twice
        instructions = subprocess.Popen(['pdflatex', out_path])
        instructions.communicate()
        instructions = subprocess.Popen(['pdflatex', out_path])
        instructions.communicate()
        # Delete residus of export
        os.unlink(out_path)
        os.unlink(out_path.replace('.tex', '.aux'))
        os.unlink(out_path.replace('.tex', '.log'))
        os.unlink(out_path.replace('.tex', '.out'))
        os.unlink(out_path.replace('.tex', '.toc'))
    def add_chapter(self, pages=None, name=None,
                    type='section', includepdf=True):
        # Got to flesh this out
        if pages is None:
            pages = str(pages)

        name = str(name)
        
        new_text = '\\phantomsection'
        
        if type == 'subsection':
            new_text += '\n\\addcontentsline{toc}{subsection}{' +\
                name + '}'
        elif type == 'section':
            new_text += '\n\\addcontentsline{toc}{section}{' +\
                name + '}'
        if includepdf:
            new_text += '\n\\includepdf[pages=' + pages[0] + '-' +\
                pages[1] + ', pagecommand={}]{' + self.source + '}'
        self.update_content(new_text)

    def def_source(self, source, path='./'):
        self.source = path + source

    def print_content(self):
        print(self.content)

def prepare_toc(filepath):
    f = open(filepath, 'r')
    count = 0
    doc_division = []
    pages_vec = []
    name_vec = []

    for line in f:
        if count < 1:
            source = line
            source = source.strip()
            count += 1
        else:
            parts = line.split(' ')
            ctype = parts[0]
            doc_division.append(ctype)
            pages = [parts[1], parts[2]]
            pages_vec.append(pages)
            name_parts = parts[3:]
            name = ''
            for word in name_parts:
                name += word + ' '
            name = name.strip()
            name_vec.append(name)

    include_vec = []
    for i in range(0, len(doc_division)-1):
        if doc_division[i+1] == 'subsection' and \
           doc_division[i] == 'section':
            include_vec.append(False)
        elif doc_division[i] == 'subsection':
            include_vec.append(True)
        else:
            include_vec.append(True)
    include_vec.append(True)

    docu = ltx_doc()
    docu.def_source(source)
    for i in range(len(doc_division)):
        docu.add_chapter(pages=pages_vec[i], name=name_vec[i],
                         includepdf=include_vec[i],
                         type=doc_division[i])
    docu.export_pdf()

if __name__ == '__main__':
    filepath = os.path.abspath(sys.argv[1])
    print(filepath)
    prepare_toc(filepath)

    
